import PropTypes from 'prop-types';
import Head from 'next/head';

function BlankLayout({children, title}) {
  return (
    <>
      <Head>
        <title>
          {title}
          {' '}
          | Rexberry 🐞
        </title>
      </Head>
      {children}
    </>
  );
}

BlankLayout.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default BlankLayout;
