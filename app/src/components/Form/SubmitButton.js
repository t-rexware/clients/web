import PropTypes from 'prop-types';
import {useFormikContext} from 'formik';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';

function SubmitButton({label, ...props}) {
  const {isSubmitting} = useFormikContext();
  const Loading = <CircularProgress color="inherit" size={30} thickness={5} />;

  return (
    <Button
      type="submit"
      size="large"
      color="primary"
      variant="contained"
      fullWidth
      {...props}
    >
      {isSubmitting ? Loading : label}
    </Button>
  );
}

SubmitButton.propTypes = {
  label: PropTypes.string.isRequired,
};

export default SubmitButton;
