import PropTypes from 'prop-types';
import TextField from './TextField';

function PasswordField({name, ...props}) {

  return (
    <TextField
      name={name}
      type="password"
      label="Contraseña"
      autoComplete="current-password"
      {...props}
    />
  );
}

PasswordField.propTypes = {
  name: PropTypes.string.isRequired,
};

export default PasswordField;
