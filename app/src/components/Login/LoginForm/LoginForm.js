import PropTypes from 'prop-types';
import {makeStyles} from '@material-ui/core/styles';

import PasswordField from 'components/Form/PasswordField';
import EmailField from 'components/Form/EmailField';
import SubmitButton from 'components/Form/SubmitButton';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

function LoginForm({handleSubmit, status}) {
  const classes = useStyles();
  const disabled = status === 'loading';

  return (
    <form className={classes.root} onSubmit={handleSubmit} noValidate>
      <EmailField
        name="email"
        required
        autoFocus
        disabled={disabled}
      />
      <PasswordField
        name="password"
        required
        disabled={disabled}
      />
      <SubmitButton
        label="Ingresar"
        className={classes.submit}
      />
    </form>
  );
}

LoginForm.defaultProps = {
  handleSubmit: () => {},
  status: '',
};

LoginForm.propTypes = {
  handleSubmit: PropTypes.func,
  status: PropTypes.string,
};

export default LoginForm;
