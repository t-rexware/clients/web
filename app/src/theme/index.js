import {createTheme} from '@material-ui/core/styles';
import {red} from '@material-ui/core/colors';

const theme = createTheme({
  palette: {
    primary: {
      main: '#4088C7',
    },
    secondary: {
      main: '#11A64F',
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
  },
});

export default theme;
