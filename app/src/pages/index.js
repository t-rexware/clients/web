import {Box, Container, Typography} from '@material-ui/core';

function Home() {
  return (
    <Container maxWidth="sm">
      <Box my={4} textAlign="center">
        <Typography variant="h4" component="h1" gutterBottom>
          Web App
        </Typography>
      </Box>
    </Container>
  );
}

export default Home;
