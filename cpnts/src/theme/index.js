import { createTheme } from '@material-ui/core/styles';

const theme = createTheme({
  palette: {
    primary: {
      main: '#4088C7',
    },
    secondary: {
      main: '#11A64F',
    },
    error: {
      main: '#FF1744',
    },
    background: {
      default: '#fff',
    },
  },
});

export default theme;
