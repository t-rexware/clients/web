import React from 'react';
import PropTypes from 'prop-types';
import { useField } from 'formik';
import { TextField as MuiTextField } from '@material-ui/core';

function TextField({ name, label, ...props }) {
  const [field, meta] = useField(name);
  const error = meta.touched && meta.error;

  return (
    <MuiTextField
      name={name}
      label={label}
      value={field.value}
      error={!!error}
      onChange={field.onChange}
      helperText={error || null}
      variant="outlined"
      margin="normal"
      InputProps={{
        onBlur: field.onBlur,
      }}
      fullWidth
      {...props}
    />
  );
}

TextField.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
};

export default TextField;
