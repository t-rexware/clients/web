import React from 'react';
import { Formik } from 'formik';
import SubmitButton from './SubmitButton';

export default {
  title: 'SubmitButton',
  component: SubmitButton,
  decorators: [
    (Story) => (
      <Formik initialValues={{label: '' }} onSubmit={() => {}}>
        <Story />
      </Formik>
    ),
  ],
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Template = (args) => <SubmitButton {...args} />;

export const SubmitButtonPrimary = Template.bind({});
SubmitButtonPrimary.args = {
  label: 'Submit',
  color: 'primary',
};
