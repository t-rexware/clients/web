import React from 'react';
import { Formik } from 'formik';
import PasswordField from './PasswordField';

export default {
  title: 'PasswordField',
  component: PasswordField,
  decorators: [
    (Story) => (
      <Formik initialValues={{ name: '' }} onSubmit={() => {}}>
        <Story />
      </Formik>
    ),
  ],
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Template = (args) => <PasswordField {...args} />;

export const PasswordFieldPrimary = Template.bind({});
PasswordFieldPrimary.args = {
  name: 'name',
  label: 'Contraseña',
};
