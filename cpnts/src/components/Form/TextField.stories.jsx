import React from 'react';
import { Formik } from 'formik';
import TextField from './TextField';

export default {
  title: 'TextField',
  component: TextField,
  decorators: [
    (Story) => (
      <Formik initialValues={{ name: '' }} onSubmit={() => {}}>
        <Story />
      </Formik>
    ),
  ],
};

// eslint-disable-next-line react/jsx-props-no-spreading
const Template = (args) => <TextField {...args} />;

export const TextFieldPrimary = Template.bind({});
TextFieldPrimary.args = {
  name: 'name',
  label: 'Nombre de usuario',
};
