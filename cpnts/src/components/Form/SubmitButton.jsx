import React from 'react';
import PropTypes from 'prop-types';
import { useFormikContext } from 'formik';
import CircularProgress from '@material-ui/core/CircularProgress';
import Button from '@material-ui/core/Button';

function SubmitButton({ label, color, ...props }) {
  const { isSubmitting } = useFormikContext();
  const Loading = <CircularProgress color="inherit" size={30} thickness={5} />;

  return (
    <Button
      type="submit"
      size="large"
      color={color}
      variant="contained"
      fullWidth
      {...props}
    >
      {isSubmitting ? Loading : label}
    </Button>
  );
}

SubmitButton.defaultProps = {
  color: 'primary',
};

SubmitButton.propTypes = {
  label: PropTypes.string.isRequired,
  color: PropTypes.string,
};

export default SubmitButton;
