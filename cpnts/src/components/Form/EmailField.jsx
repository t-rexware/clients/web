import React from 'react';
import PropTypes from 'prop-types';
import TextField from './TextField';

function EmailField({ name, ...props }) {
  return (
    <TextField
      name={name}
      type="email"
      label="Correo electrónico"
      autoComplete="email"
      {...props}
    />
  );
}

EmailField.propTypes = {
  name: PropTypes.string.isRequired,
};

export default EmailField;
